FROM alpine
# lein clojure

# install curl
RUN apk --no-cache add curl

# install sudo
RUN apk add --no-cache su-exec

# install bash
RUN apk add --no-cache bash

# install java
RUN apk add --no-cache openjdk11

# Install clojure
RUN curl -O https://download.clojure.org/install/linux-install-1.10.1.536.sh
RUN chmod +x linux-install-1.10.1.536.sh
RUN su-exec ./linux-install-1.10.1.536.sh

# Install lein
RUN curl https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein >/usr/local/bin/lein
RUN chmod +x /usr/local/bin/lein

COPY project.clj .
RUN lein deps

RUN apk --no-cache add --update git
RUN apk --no-cache add --update nodejs npm

